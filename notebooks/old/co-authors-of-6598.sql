﻿select count(*), person_id, last_name, first_name from authorship, persons 
where persons.id = person_id
and bib_id in
(
select bib_id from authorship where person_id = 6598
)
group by person_id, last_name, first_name
order by 1 desc