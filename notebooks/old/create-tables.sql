-- Table: bib_entries

-- DROP TABLE bib_entries;

CREATE TABLE bib_entries
(
  id serial NOT NULL,
  import_id text,
  import_source text,
  title text,
  authors text,
  pub_type text,
  pub_date text,
  pub_year integer,
  abstract text,
  doi text,
  isbn text,
  issn text,
  key_concepts text,
  institutions text,
  issue text,
  journal_name text,
  language text,
  location text,
  methodology text,
  subject_headings text,
  pub_month text,
  original_title text,
  pages text,
  pmid text,
  population_group text,
  publisher text,
  cited_references text,
  source text,
  special_issue_title text,
  book_series text,
  chapter_title text,
  volume text,
  CONSTRAINT bib_entries_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bib_entries
  OWNER TO bibuser;


-- Table: persons

-- DROP TABLE persons;

CREATE TABLE persons
(
  first_name text,
  last_name text,
  id serial NOT NULL,
  CONSTRAINT persons_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons
  OWNER TO bibuser;


-- Table: authorship

-- DROP TABLE authorship;

CREATE TABLE authorship
(
  id serial NOT NULL,
  person_id integer,
  bib_id integer,
  CONSTRAINT authorship_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE authorship
  OWNER TO bibuser;

