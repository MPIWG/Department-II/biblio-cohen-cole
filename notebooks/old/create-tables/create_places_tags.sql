-- Table: places_tags

-- DROP TABLE places_tags;

CREATE TABLE places_tags
(
  id serial NOT NULL,
  place_id integer,
  tag text,
  CONSTRAINT places_tags_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE places_tags
  OWNER TO bibuser;
