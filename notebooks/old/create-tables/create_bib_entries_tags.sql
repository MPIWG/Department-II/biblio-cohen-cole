﻿-- Table: bib_entries_tags

-- DROP TABLE bib_entries_tags;

CREATE TABLE bib_entries_tags
(
  id serial NOT NULL,
  bib_entries_id integer,
  tag text,
  CONSTRAINT bib_entries_tags_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE bib_entries_tags
  OWNER TO bibuser;
