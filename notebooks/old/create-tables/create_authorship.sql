﻿-- Table: authorship

-- DROP TABLE authorship;

CREATE TABLE authorship
(
  id serial NOT NULL,
  person_id integer,
  bib_id integer,
  CONSTRAINT authorship_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE authorship
  OWNER TO bibuser;
