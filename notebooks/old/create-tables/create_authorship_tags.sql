﻿-- Table: authorship_tags

-- DROP TABLE authorship_tags;

CREATE TABLE authorship_tags
(
  id serial NOT NULL,
  authorship_id integer,
  tag text,
  CONSTRAINT authorship_tags_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE authorship_tags
  OWNER TO bibuser;
