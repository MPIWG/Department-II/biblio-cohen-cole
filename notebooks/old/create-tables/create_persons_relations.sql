﻿-- Table: persons_relations

-- DROP TABLE persons_relations;

CREATE TABLE persons_relations
(
  id serial NOT NULL,
  person_id_src integer,
  person_id_dest integer,
  type text,
  notes text,
  valid_to date,
  valid_from date,
  CONSTRAINT persons_relations_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons_relations
  OWNER TO bibuser;
