-- Table: places_places

-- DROP TABLE places_places;

CREATE TABLE places_places
(
  id serial NOT NULL,
  src_place_id integer NOT NULL,
  tar_place_id integer NOT NULL,
  relation_type text,
  CONSTRAINT places_places_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE places_places
  OWNER TO bibuser;
