﻿-- Table: persons

-- DROP TABLE persons;

CREATE TABLE persons
(
  first_name text,
  last_name text,
  id serial NOT NULL,
  same_as integer,
  CONSTRAINT persons_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons
  OWNER TO bibuser;
