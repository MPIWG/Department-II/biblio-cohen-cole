﻿-- Table: persons_tags

-- DROP TABLE persons_tags;

CREATE TABLE persons_tags
(
  id serial NOT NULL,
  persons_id integer,
  tag text,
  CONSTRAINT persons_tags_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons_tags
  OWNER TO bibuser;
