-- Table: places

-- DROP TABLE places;

CREATE TABLE places
(
  id serial NOT NULL,
  name text NOT NULL,
  type text,
  CONSTRAINT places_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE places
  OWNER TO bibuser;
