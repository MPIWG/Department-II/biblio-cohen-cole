-- Table: persons_places

-- DROP TABLE persons_places;

CREATE TABLE persons_places
(
  id serial NOT NULL,
  person_id integer NOT NULL,
  place_id integer NOT NULL,
  relation_type text,
  bib_id integer,
  CONSTRAINT persons_places_pkey PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE persons_places
  OWNER TO bibuser;
