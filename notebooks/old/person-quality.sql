﻿-- select last_name, first_name from persons order by last_name

select count(*), last_name from persons 
group by last_name 
having count(*) > 1
order by 1 desc